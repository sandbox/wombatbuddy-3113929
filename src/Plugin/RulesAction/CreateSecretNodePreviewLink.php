<?php

namespace Drupal\rules_secret_preview_link\Plugin\RulesAction;

use Drupal\rules\Core\RulesActionBase;
use Drupal\Core\Plugin\ContainerFactoryPluginInterface;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\Core\Site\Settings;
use Drupal\Core\Url;

/**
 * Provides an 'Create secret node preview link' action.
 *
 * @RulesAction(
 *   id = "rules_secret_preview_link_create",
 *   label = @Translation("Create secret node preview link"),
 *   category = @Translation("Data"),
 *   context = {
 *     "nid" = @ContextDefinition("integer",
 *       label = @Translation("Node id"),
 *       description = @Translation("The id of the node that should be previewed with a secret link.")
 *     ),
 *   },
 *   provides = {
 *     "secret_node_preview_link" = @ContextDefinition("string",
 *        label = @Translation("Secret node preview link")
 *      ),
 *    }
 * )
 */
class CreateSecretNodePreviewLink extends RulesActionBase implements ContainerFactoryPluginInterface {

  /**
   * The settings service.
   *
   * @var \Drupal\Core\Site\Settings
   */
  protected $settings;

  /**
   * Constructs a CreateSecretNodePreviewLink object.
   *
   * @param array $configuration
   *   A configuration array containing information about the plugin instance.
   * @param string $plugin_id
   *   The plugin ID for the plugin instance.
   * @param mixed $plugin_definition
   *   The plugin implementation definition.
   * @param \Drupal\Core\Site\Settings $settings
   *   The settings.
   */
  public function __construct(array $configuration, $plugin_id, $plugin_definition, Settings $settings) {
    parent::__construct($configuration, $plugin_id, $plugin_definition);
    $this->settings = $settings;
  }

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container, array $configuration, $plugin_id, $plugin_definition) {
    return new static(
      $configuration,
      $plugin_id,
      $plugin_definition,
      $container->get('settings')
    );
  }

  /**
   * Create secret node preview link.
   *
   * @param int $nid
   *   The id of the node that should be previewed with a secret link.
   */
  protected function doExecute($nid) {
    // Generate the token.
    $token = md5($nid . $this->settings::getHashSalt());
    $parameters = [
      'node' => $nid,
      'secret_code' => $token,
    ];
    $options = ['absolute' => TRUE];
    $url = Url::fromRoute('rules_secret_preview_link.render_secret_node', $parameters, $options)->toString();
    $this->setProvidedValue('secret_node_preview_link', $url);
  }

  /**
   * {@inheritdoc}
   */
  public function refineContextDefinitions(array $selected_data) {
  }

}
