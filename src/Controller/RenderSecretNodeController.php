<?php

namespace Drupal\rules_secret_preview_link\Controller;

use Drupal\Core\Controller\ControllerBase;
use Symfony\Component\DependencyInjection\ContainerInterface;
use Drupal\node\NodeInterface;
use Symfony\Component\HttpFoundation\RedirectResponse;
use Drupal\Core\Url;

/**
 * Class RenderSecretNodeController.
 */
class RenderSecretNodeController extends ControllerBase {

  /**
   * The entity_type.manager service.
   *
   * @var \Drupal\Core\Entity\EntityTypeManagerInterface
   */
  protected $entityTypeManager;

  /**
   * The settings service.
   *
   * @var \Drupal\Core\Site\Settings
   */
  protected $settings;

  /**
   * {@inheritdoc}
   */
  public static function create(ContainerInterface $container) {
    $instance = parent::create($container);
    $instance->entityTypeManager = $container->get('entity_type.manager');
    $instance->settings = $container->get('settings');
    return $instance;
  }

  /**
   * Check the secret code and return a rendered node if it's alright.
   *
   * @return array
   *   Return rendered array of a node.
   */
  public function renderSecretNode(NodeInterface $node, $secret_code) {
    // Generate the token.
    $token = md5($node->id() . $this->settings::getHashSalt());

    if ($secret_code != $token) {
      $url = Url::fromRoute('system.403');
      return new RedirectResponse($url->toString());
    }
    else {
      return $this->entityTypeManager->getViewBuilder('node')->view($node, 'full');
    }
  }

}
